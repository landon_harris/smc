#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
	#AutoIt3Wrapper_Version=Beta
	#AutoIt3Wrapper_Outfile_x64=..\SMC_ListDiff.exe
	#AutoIt3Wrapper_Res_Language=1033
	#AutoIt3Wrapper_AU3Check_Parameters=-w 1 -w 2 -w 3 -w 4 -w 5 -w 6 -q
	#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 162,
	#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 164,
	#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 169,
	#AutoIt3Wrapper_Run_Tidy=y
	#Tidy_Parameters=/reel /ri /sci 0 /sf
	#AutoIt3Wrapper_Run_Au3Stripper=y
	#Au3Stripper_Parameters=/so /Beta /mi 9
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#Region Pragma
	#pragma compile(Icon, ..\Include\SMC_ListDiff.ico)
	#pragma compile(ExecLevel, asInvoker)
	#pragma compile(UPX, False)
	#pragma compile(AutoItExecuteAllowed, False)
	#pragma compile(Compression, 0)
	#pragma compile(FileDescription, Skyrim Mod Combiner List Diff)
	#pragma compile(ProductName, SMC)
	#pragma compile(ProductVersion, 1.3)
	#pragma compile(FileVersion, 1.3.6.0)
	#pragma compile(LegalCopyright, � dr.)
#EndRegion Pragma

#include <File.au3>
#include <Array.au3>
#include <Misc.au3>

Global $InputArray, $OutputArray

If FileExists(@AppDataDir & '\Dropbox\info.json') Then $DropboxInfo = FileReadToArray(@AppDataDir & '\Dropbox\info.json')
If Not @error Then
	$DropboxPath = StringTrimLeft($DropboxInfo[0], StringInStr($DropboxInfo[0], '"path"') + 8)
	$DropboxPath = StringReplace(StringLeft($DropboxPath, StringInStr($DropboxPath, '"') - 1), '\\', '\')
EndIf
;~ ConsoleWrite($DropboxPath & @CRLF)
If _IsPressed(11) And FileExists($DropboxPath & '\=SMC\List\SMC.ini') Then
	$Input = $DropboxPath & '\=SMC\List\SMC.ini'
Else
	$Input = FileOpenDialog('Input File Select', @ScriptDir, 'List files (*.lst;*.ini)', 3)
EndIf
;~ ConsoleWrite($Input & @CRLF)
If FileExists($Input) = 0 Then Exit
If StringRight($Input, 4) = '.lst' Then
	Global $7zExe = $DropboxPath & '\=SMC\x64\7z.exe'
	DirCreate(@TempDir & '\SMC_List')
	While 1
		If RunProcessing($Input, @TempDir & '\SMC_List') = 0 Then
			$InputArray = FileReadToArray(@TempDir & '\SMC_List' & '\SMC.ini')
			If @error Or $InputArray[0] <> '[Skyrim]' Then
				Exit
			Else
				ExitLoop
			EndIf
		Else
			Exit
		EndIf
	WEnd
	DirRemove(@TempDir & '\SMC_List', 1)
ElseIf StringRight($Input, 4) = '.ini' Then
	$InputArray = FileReadToArray($Input)
Else
	Exit
EndIf
While 1
	$ArraySearchResult = _ArraySearch($InputArray, '[', 0, 0, 0, 1)
	If $ArraySearchResult = -1 Then
		ExitLoop
	Else
		_ArrayDelete($InputArray, $ArraySearchResult)
	EndIf
WEnd
_ArrayReverse($InputArray)
$InputArray = _ArrayUnique($InputArray, 0, 0, 0, 0)
_ArraySort($InputArray)
$Input = StringTrimRight(StringTrimLeft($Input, StringInStr($Input, '\', 0, -1)), 4)
If _IsPressed(11) And FileExists(@ScriptDir & '\_PathList.ini') Then
	$Output = @ScriptDir & '\_PathList.ini'
Else
	$Output = FileOpenDialog('Output File Select', @ScriptDir, 'List files (*.ini;*.txt)', 3)
EndIf
;~ ConsoleWrite($Output & @CRLF)
If FileExists($Output) = 0 Then Exit
$OutputArray = FileReadToArray($Output)
$OutputArray = _ArrayUnique($OutputArray, 0, 0, 0, 0)
_ArraySort($OutputArray)
$Output = StringTrimRight(StringTrimLeft($Output, StringInStr($Output, '\', 0, -1)), 4)

For $i = 0 To UBound($InputArray) - 1
	For $j = 0 To UBound($OutputArray) - 1
		If $InputArray[$i] = $OutputArray[$j] Then
			_ArrayDelete($OutputArray, $j)
			ExitLoop
		EndIf
	Next
Next
_FileWriteFromArray(@ScriptDir & '\_' & $Input & '-' & $Output & '.ini', $OutputArray, 0)

Func RunProcessing($InputPath, $OutputPath)
	If StringCompare(StringRight($OutputPath, 1), '\') <> 0 Then $OutputPath &= '\'
	Local $PID = Run('"' & $7zExe & '" ' & 'x' & ' "' & $InputPath & '" ' & '-o' & '"' & $OutputPath & '" ' & '-y', @ScriptDir, @SW_HIDE, $STDERR_MERGED)
	While ProcessExists($PID)
		Local $7zProcessStats = ProcessGetStats($PID, 1)
		If $7zProcessStats = 0 Then ExitLoop
		Sleep(100)
	WEnd
	ProcessWaitClose($PID)
	Return 0
EndFunc   ;==>RunProcessing
