#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
	#AutoIt3Wrapper_Version=Beta
	#AutoIt3Wrapper_Outfile_x64=..\SMC_ListFolder.exe
	#AutoIt3Wrapper_Res_Language=1033
	#AutoIt3Wrapper_AU3Check_Parameters=-w 1 -w 2 -w 3 -w 4 -w 5 -w 6 -q
	#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 162,
	#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 164,
	#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 169,
	#AutoIt3Wrapper_Run_Tidy=y
	#Tidy_Parameters=/reel /ri /sci 0 /sf
	#AutoIt3Wrapper_Run_Au3Stripper=y
	#Au3Stripper_Parameters=/so /Beta /mi 9
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#Region Pragma
	#pragma compile(Icon, ..\Include\SMC_ListFolder.ico)
	#pragma compile(ExecLevel, asInvoker)
	#pragma compile(UPX, False)
	#pragma compile(AutoItExecuteAllowed, False)
	#pragma compile(Compression, 0)
	#pragma compile(FileDescription, Skyrim Mod Combiner List Folder)
	#pragma compile(ProductName, SMC)
	#pragma compile(ProductVersion, 1.3)
	#pragma compile(FileVersion, 1.3.6.0)
	#pragma compile(LegalCopyright, � dr.)
#EndRegion Pragma

#include <File.au3>

Global $ListPathFile, $List, $ListPathFirst, $ListPathFrom, $ListPathTo, $ListPathName
$ListPathFile = FileOpen('_PathList.ini', 10)
$List = _FileListToArrayRec(@ScriptDir, '*', 1, 1, 1, 1)
For $i = 1 To $List[0]
	$ListPathTo = StringLeft($List[$i], StringInStr($List[$i], '\', 0, -1))
	$ListPathFirst = StringLeft($List[$i], StringInStr($List[$i], '\', 0, 1) - 1)
	$ListPathFrom = StringTrimLeft($List[$i], StringInStr($List[$i], '\', 0, 1))
	$ListPathName = StringTrimLeft(@ScriptDir, StringInStr(@ScriptDir, '\', 0, -1))
	If $ListPathName = '_' Then $ListPathName = StringTrimLeft(StringLeft(@ScriptDir, StringInStr(@ScriptDir, '\', 0, -1) - 1), StringInStr(@ScriptDir, '\', 0, -2))
	If $ListPathTo Then
		If StringInStr($ListPathTo, 'Screenshot') = 0 And StringLeft($ListPathTo, StringInStr($ListPathTo, '\', 0, 1)) <> 'images\' And StringLeft($ListPathTo, StringInStr($ListPathTo, '\', 0, 1)) <> 'Fomod\' And StringLeft($ListPathTo, StringInStr($ListPathTo, '\', 0, 1)) <> '_\' And StringInStr('txt ini inf exe .db .7z rar zip au3 jpg', StringRight($List[$i], 3)) = 0 Then
			If StringLeft($List[$i], 7) = 'meshes\' Or StringLeft($List[$i], 9) = 'textures\' Or StringLeft($List[$i], 10) = 'interface\' Or StringLeft($List[$i], 6) = 'music\' Or StringLeft($List[$i], 8) = 'scripts\' Or StringLeft($List[$i], 5) = 'SKSE\' Or StringLeft($List[$i], 6) = 'sound\' Or StringLeft($List[$i], 8) = 'strings\' Then
				FileWriteLine($ListPathFile, $ListPathName & '|' & $ListPathFirst & '|' & $ListPathFrom & '|' & $ListPathTo)
			Else
				FileWriteLine($ListPathFile, $ListPathName & '|' & $ListPathFirst & '|' & $ListPathFrom & '|' & StringTrimLeft($ListPathTo, StringInStr($ListPathTo, '\', 0, 1)))
			EndIf
		EndIf
	Else
		If StringLeft($List[$i], 9) <> 'SMC' And StringInStr('txt ini inf exe .db .7z rar zip au3 jpg', StringRight($List[$i], 3)) = 0 Then
			FileWriteLine($ListPathFile, $ListPathName & '|' & $List[$i] & '||')
		EndIf
	EndIf
Next
FileClose($ListPathFile)
