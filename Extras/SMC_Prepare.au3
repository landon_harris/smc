#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
	#AutoIt3Wrapper_Version=Beta
	#AutoIt3Wrapper_Outfile_x64=..\SMC_Prepare.exe
	#AutoIt3Wrapper_Res_Language=1033
	#AutoIt3Wrapper_AU3Check_Parameters=-w 1 -w 2 -w 3 -w 4 -w 5 -w 6 -q
	#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 162,
	#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 164,
	#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 169,
	#AutoIt3Wrapper_Run_Tidy=y
	#Tidy_Parameters=/reel /ri /sci 0 /sf
	#AutoIt3Wrapper_Run_Au3Stripper=y
	#Au3Stripper_Parameters=/so /Beta /mi 9
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#Region Pragma
	#pragma compile(Icon, ..\Include\SMC_Prepare.ico)
	#pragma compile(ExecLevel, asInvoker)
	#pragma compile(UPX, False)
	#pragma compile(AutoItExecuteAllowed, False)
	#pragma compile(Compression, 0)
	#pragma compile(FileDescription, Skyrim Mod Combiner Preparator)
	#pragma compile(ProductName, SMC)
	#pragma compile(ProductVersion, 1.3)
	#pragma compile(FileVersion, 1.3.6.0)
	#pragma compile(LegalCopyright, � dr.)
#EndRegion Pragma

#include <File.au3>
#include <..\Include\FileOperations.au3>

Global $ScriptDir = (@Compiled = 0 ? StringLeft(@ScriptDir, StringInStr(@ScriptDir, '\', 0, -1) - 1) : @ScriptDir)
Global $7zExe = $ScriptDir & '\' & (@OSArch = 'X86' ? 'x86' : 'x64') & '\7z.exe'
Global $ListPath = $ScriptDir & '\List\'
Global $ListNames[] = ['SMC', 'SMC_Light']
Global $ListContents
For $i = 0 To UBound($ListNames) - 1
	_FileReadToArray($ListPath & $ListNames[$i] & '.ini', $ListContents, 0)
	_ArrayReverse($ListContents)
	$ListContents = _ArrayUnique($ListContents, 0, 0, 0, 0)
	_ArrayReverse($ListContents)
	_FileWriteFromArray($ListPath & $ListNames[$i] & '.ini', $ListContents, 0)
	RunWait('"' & $7zExe & '" u "' & $ListPath & $ListNames[$i] & '.lst" "' & $ListPath & $ListNames[$i] & '.ini" ', $ScriptDir, @SW_HIDE)
Next
$InputINI = $ScriptDir & '\settings.ini'
$TempFile = _TempFile(@TempDir, '~', '.smc')
IniWriteSection($InputINI, 'Checksums', '')
$FileList = _FileListToArray($ScriptDir & '\Pics\', '*.png', 1)
If $FileList <> 0 Then
	For $i = 1 To $FileList[0]
		IniWrite($InputINI, 'Checksums', 'Pics\' & $FileList[$i], FileGetSize($ScriptDir & '\Pics\' & $FileList[$i]))
	Next
EndIf
$FileList = _FileListToArray($ScriptDir & '\Lang\', '*.lng', 1)
If $FileList <> 0 Then
	For $i = 1 To $FileList[0]
		IniWrite($InputINI, 'Checksums', 'Lang\' & $FileList[$i], FileGetSize($ScriptDir & '\Lang\' & $FileList[$i]))
	Next
EndIf
$FileList = _FileListToArray($ScriptDir & '\List\', '*.lst', 1)
If $FileList <> 0 Then
	For $i = 1 To $FileList[0]
		IniWrite($InputINI, 'Checksums', 'List\' & $FileList[$i], FileGetSize($ScriptDir & '\List\' & $FileList[$i]))
	Next
EndIf
IniWrite($InputINI, 'System', 'TempDir', '')
IniWrite($InputINI, 'System', 'VersionCurrent', '')
IniWrite($InputINI, 'System', 'PreferredLanguage', '')
IniWrite($InputINI, 'System', 'PreferredInputPath', '')
IniWrite($InputINI, 'System', 'PreferredOutputPath', '')
IniWrite($InputINI, 'System', 'ColorFoundBoth', '')
IniWrite($InputINI, 'System', 'ColorFoundArchived', '')
IniWrite($InputINI, 'System', 'ColorFoundExtracted', '')
IniWrite($InputINI, 'System', 'ColorFoundUnsupported', '')
IniWrite($InputINI, 'System', 'ColorFoundNone', '')
IniWrite($InputINI, 'System', 'ColorContainsBoth', '')
IniWrite($InputINI, 'System', 'ColorContainsESP', '')
IniWrite($InputINI, 'System', 'ColorContainsScripts', '')
IniWrite($InputINI, 'System', 'CleanInstall', '')
IniWrite($InputINI, 'System', 'DDSoptimize', '')
IniWrite($InputINI, 'System', 'BSAoptUnpack', '')
IniWrite($InputINI, 'System', 'BSAoptPack', '')
IniWrite($InputINI, 'System', 'CreateArchive', '')
IniWrite($InputINI, 'System', 'UseCompression', '')
IniWrite($InputINI, 'System', 'NoAutoUpdate', '')
IniWrite($InputINI, 'System', 'NoLogging', '')
IniWrite($InputINI, 'System', 'NoParallax', '')
IniWrite($InputINI, 'System', 'ForceEnableDLC', '')
IniWrite($InputINI, 'System', 'EditorMode', '')
IniWrite($InputINI, 'System', 'PreviousOutputPath', '')
IniWrite($InputINI, 'System', 'PreviousWindowSettings', '')
IniWrite($InputINI, 'System', 'PreviousNNMPath', '')
IniDelete($InputINI, 'Defined Paths')

If FileExists($InputINI) Then
	FileDelete($TempFile)
	Global $FileMoveRetries = 0, $FileMoveResult = 0, $iniLineLongest = 0, $iniLine, $iniFileTemp, $iniFileIn, $iniFileOut
	IniFormatSection($InputINI, 'System')
	IniFormatSection($InputINI, 'Checksums')
	IniFormatSection($InputINI, 'Defined Paths')
	IniFormatSection($InputINI, 'Compatibility Patches')
	$iniFileIn = FileOpen($InputINI, 0)
	If $iniFileIn = -1 Then Exit
	$iniLineLongest = 0
	While 1
		$iniLine = FileReadLine($iniFileIn)
		If @error = -1 Then ExitLoop
		$iniLine = StringSplit($iniLine, '=')
		If $iniLine[0] > 1 And StringLen(StringStripWS($iniLine[1], 3)) > $iniLineLongest Then $iniLineLongest = StringLen(StringStripWS($iniLine[1], 3))
	WEnd
	FileClose($iniFileIn)
	$iniFileTemp = FileOpen($TempFile, 9)
	$iniFileOut = FileOpen($InputINI, 0)
	If $iniFileTemp = -1 Or $iniFileOut = -1 Then Exit
	While 1
		$iniLine = FileReadLine($iniFileOut)
		If @error = -1 Then ExitLoop
		$iniLine = StringSplit($iniLine, '=')
		$iniLine[1] = StringStripWS($iniLine[1], 3)
		If $iniLine[0] > 1 Then
			For $i = 1 To $iniLineLongest - StringLen($iniLine[1])
				$iniLine[1] &= ' '
			Next
			$iniLine[1] &= ' = ' & StringReplace(StringReplace((StringRight(StringStripWS($iniLine[2], 3), 1) = ';' ? StringTrimRight(StringStripWS($iniLine[2], 3), 1) : StringRight(StringStripWS($iniLine[2], 3), 1) = '|') ? StringTrimRight(StringStripWS($iniLine[2], 3), 1) : StringStripWS($iniLine[2], 3), ';;', ';'), '; ;', ';')
		EndIf
		FileWriteLine($iniFileTemp, $iniLine[1])
	WEnd
	FileClose($iniFileTemp)
	FileClose($iniFileOut)
	Do
		$FileMoveResult = FileMove($TempFile, $InputINI, 9)
		If $FileMoveResult <> 1 Then
			$FileMoveRetries += 1
			Sleep(100)
		EndIf
		If $FileMoveRetries > 10 Then Exit
	Until $FileMoveResult = 1
EndIf

Func IniFormatSection($InputINI, $Section)
	Local $InputINISection = IniReadSection($InputINI, $Section)
	If Not @error Then
		If IniDelete($InputINI, $Section) = 0 Then Return -1
		$iniLineLongest = 0
		For $i = 1 To $InputINISection[0][0]
			If StringLen(StringStripWS($InputINISection[$i][0], 3)) > $iniLineLongest Then $iniLineLongest = StringLen(StringStripWS($InputINISection[$i][0], 3))
		Next
		Local $iniFileTemp = FileOpen($TempFile, 9)
		If $iniFileTemp = -1 Then Return -1
		FileWriteLine($iniFileTemp, '[' & $Section & ']')
		For $i = 1 To $InputINISection[0][0]
			If $InputINISection[0][0] > 1 Then
				For $i1 = 1 To $iniLineLongest - StringLen($InputINISection[$i][0])
					$InputINISection[$i][0] &= ' '
				Next
			EndIf
			FileWriteLine($iniFileTemp, $InputINISection[$i][0] & ' = ' & (StringRight(StringStripWS($InputINISection[$i][1], 3), 1) = ';' ? StringTrimRight(StringStripWS($InputINISection[$i][1], 3), 1) : StringRight(StringStripWS($InputINISection[$i][1], 3), 1) = '|' ? StringTrimRight(StringStripWS($InputINISection[$i][1], 3), 1) : StringStripWS($InputINISection[$i][1], 3)))
		Next
		FileClose($iniFileTemp)
	EndIf
EndFunc   ;==>IniFormatSection
