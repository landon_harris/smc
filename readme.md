# What is SMC (Skyrim Mod Combiner)?

SMC is a free, fast and light-weight mod combiner for Skyrim, designed to be as universal and user-friendly as possible.
> SMC was initially made as extended GUI wrapper for a batch-file from [Texture Pack Combiner](http://skyrim.nexusmods.com/mods/20801) by **Cestral**,
but uses its own updated lists now for compatibility with newer and additional mods.  
  
> If you like what I do you can buy me a drink by clicking this pretty button here:  [![Donate with PayPal Button](http://i.imgur.com/q5Azoli.png "Donate with PayPal")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LVHX9P9F7UHHQ)

---

   [![SMC Screenshot](http://s4.postimg.org/lqqf6crst/SMC_Screenshot.png "SMC Screenshot")](http://s4.postimg.org/lqqf6crst/SMC_Screenshot.png)

---

## SMC features:

* Written fully in AutoIt, so it is **Windows-only**  
* Comes with a **[7-Zip](http://www.7-zip.org/)** implemented  
* Uses **SQLite** as an internal database  
* Works with _archived_ and/or _extracted_ mods  
* Automatically installs _compatibility patches_, if needed  
* Dynamically calculates _space required_  
* Dynamically indicates _mod availability_  
* Optionally packs the combined output into _NMM-compatible archive_  
* Optionally _optimizes_ combined textures with **[DDSopt](http://skyrim.nexusmods.com/mods/5755)** and/or _unpacks input/packs output into BSA-archive_ with **[BSAopt](http://skyrim.nexusmods.com/mods/247)** by **Ethatron**  
* Automatically creates readable _detailed log_  

---

## Downloading & updating:

* Latest SMC version can always be found in **[Downloads](https://bitbucket.org/drigger/smc/downloads)** section  
* There is also a new **[SMC homepage on Nexus](http://www.nexusmods.com/skyrim/mods/51467)** available  
* Or you can simply click **[here](https://bitbucket.org/drigger/smc/downloads/SMC_1.3.6.1.7z)**  
* And don't worry, SMC will update itself once installed (but you can still check this page every now and then, just in case)  

---

## Issues:

If you find an issue, please report it on the **[issue tracker](https://bitbucket.org/drigger/smc/issues)**.  
However, please use the search function to make sure you don't make a duplicate.  
  
Creating a new issue:  

* Make a coherent description that explains your issue in details  
* Use of pictures is welcomed to better describe your issue  
* Please make sure you've included your faulty compilation log file (it should be available in the corresponding _Logs_ folder)  

---

## Current to-do list:

* aMidianBorn Book of Silence  
* aMidianBorn Hide and Studded Hide  
* aMidianBorn Hide of the Savior  
* aMidianBorn imperial light and studded  
* aMidianBorn Iron and Banded Armor  
* aMidianBorn Scaled Armor  
* aMidianBorn Steel Armor  
* aMidianBorn stormcloak officer armour  
* aMidianBorn wolf armor and skyforge weapons  

---

## Thanks for donations from:

* Chris Soldatos  
* Jeffrey Smith  
* Joshua Baker  
* Malcolm Allison  
* Matthew Dayne  
* Mohammed Muhanna  
* Sergio Bonfiglio  
* Tomas Sanchez Gutierrez  
  
> Really, thank you all very much, guys!  

---

## Latest changes:

* > [1.3.6.1 @ 05.06.2015]  
(U) 7-Zip updated to the latest 15.03 alpha  
* > [1.3.6.0 @ 25.05.2015]  
(+) Optional parameters in settings.ini for changing default color-coding  
(U) 7-Zip updated to the latest 15.02 alpha  
* > [1.3.5.7 @ 12.05.2015]  
(U) DDSopt.ini taken from S.T.E.P. guide  
(U) SQLite.dll updated to the latest 3.8.10.0  
* > [1.3.5.6 @ 10.04.2015]  
(U) SQLite.dll updated to the latest 3.8.9.0  
* > [1.3.5.5 @ 03.04.2015]  
(U) 7-Zip updated to the latest 15.00 alpha  
* > [1.3.5.4 @ 21.03.2015]  
(E) Slightly improved tooltips and mouse cursor behavior  
(E) Compatibility with updated AutoIt components  
* > [1.3.5.3 @ 24.01.2015]  
(F) Multi-folder extraction bug introduced in previous version reverted  
(U) SQLite.dll updated to the latest 3.8.8.1  
* > [1.3.5.2 @ 19.01.2015]  
(F) SQLite query finalization bug fixed  
(U) SQLite.dll updated to the latest 3.8.8.0  
* > [1.3.5.1 @ 16.01.2015]  
(E) Improved logs  
(U) 7-Zip updated to the latest 9.38 beta  
* > [1.3.5.0 @ 29.12.2014]  
(+) Text color notifications for mods containing scripts and/or ESP  
(U) 7-Zip updated to the latest 9.36 beta  
(U) SQLite.dll updated to the latest 3.8.7.4  
* > [1.3.4.2 @ 08.12.2014]  
(U) 7-Zip updated to the latest 9.35 beta  
(U) SQLite.dll updated to the latest 3.8.7.3  
* > [1.3.4.1 @ 26.11.2014]  
(U) SQLite.dll updated to the latest 3.8.7.2  
* > [1.3.4.0 @ 10.11.2014]  
(F) Optional ForceEnableDLC parameter in settings.ini  
(U) SQLite.dll updated to the latest 3.8.7.1  
* > [1.3.3.3 @ 26.10.2014]  
(F) Tiny UI edit  
(U) SQLite.dll updated to the latest 3.8.7.0  
* > [1.3.3.2 @ 10.09.2014]  
(F) Incorrect PreviousWindowSettings behavior bug fixed  
(U) 7-Zip updated to the latest 9.34 alpha  
* > [1.3.3.1 @ 28.08.2014]  
(F) .esm-files related bug fixed  
(U) SQLite.dll updated to the latest 3.8.6.0  
* > [1.3.3.0 @ 14.08.2014]  
(F) Fatal crash on launch and other bugfixes  
* > [1.3.2.3 @ 22.07.2014]  
(+) Double-clicking mod logo now opens in-app setting editor  
(F) Files overwriting mechanism bugfix  
* > [1.3.2.2 @ 11.07.2014]  
(F) Files overwriting mechanism improvements  
(U) Spanish language updated (thanks to Shiruz)  
* > [1.3.2.1 @ 09.07.2014]  
(F) Small bugfix  
* > [1.3.2.0 @ 20.06.2014]  
(+) Optional BSAoptUnpack parameter in settings.ini and in-app setting editor  
(F) First launch bugfixes  
* > [1.3.1.0 @ 18.06.2014]  
(+) Optional NoParallax parameter in settings.ini and in-app setting editor  
(+) Spanish language added (thanks to Shiruz)  
(E) Improved logs  
(U) 7-Zip updated to the latest 9.33 alpha  
* > [1.3.0.1 @ 09.06.2014]  
(F) Compatibility with updated AutoIt components and bugfixes  
(U) SQLite.dll updated to the latest 3.8.5.0  
* > [1.3.0.0 @ 30.05.2014]  
(E) MPRESS is not used anymore for exe/dll packaging (it should, hopefully, help to get rid of at least a couple of false positive antivirus reports)  
* > [1.2.0.3 @ 15.05.2014]  
(+) Compatible list feature: file copy conditions related to other mods selection  
(F) Compatibility with updated AutoIt components and bugfixes  
(E) Lowered priority of external components processing  
* > [1.2.0.2 @ 09.04.2014]  
(F) Compatibility with updated AutoIt components and bugfixes  
(U) SQLite.dll updated to the latest 3.8.4.3  
* > [1.2.0.1 @ 31.03.2014]  
(F) Hotfix for a "variable used without being declared" error  
(U) SQLite.dll updated to the latest 3.8.4.2  
* > [1.2.0.0 @ 28.03.2014]  
(+) NMM-compatible archive creation option is now available for users without NMM installed  
(+) Option for users with NMM installed to create NMM-compatible archive directly in NMM Skyrim mods folder (available through dialog window)  
(+) Compatible patches tab in mod settings editor  
(+) DDSoptimize file skipping feature finalized  
(+) File exclusion feature for new lists format (5th parameter)  
(F) Bugfixes  
(E) NMMCreate/NMMUseCompression renamed to CreateArchive/NMMUseCompression  
* > [1.1.0.1 @ 20.03.2014]  
(+) DDSoptimize file skipping feature  
* > [1.1.0.0 @ 17.03.2014]  
(+) Old TPC-styled batch files changed to new .lst (packed .ini)  
(+) Change compatible list used by SMC option (hold ctrl during app start)  
(+) Files overwriting mechanism  
(F) Better DLC detection  
(F) Faster list initialization  
* > [1.0.0.2 @ 24.02.2014]  
(F) Fixed incompatibility with x86 OS introduced in previous version  
* > [1.0.0.1 @ 21.02.2014]  
(E) NMMUseCompression option can now only be selected if NMMCreate is enabled  
* > [1.0.0.0 @ 13.02.2014]  
(+) Main TPC batch-file auto-download  
(F) CPU high consumption bug  
* > [0.9.0.5 @ 03.02.2014]  
(F) A bit more bugfixes  
* > [0.9.0.4 @ 01.02.2014]  
(F) A couple of small bugfixes  
* > [0.9.0.3 @ 01.02.2014]  
(+) NMM detection logic enhanced  
* > [0.9.0.2 @ 01.02.2014]  
(+) NMM detection status in logs  
* > [0.9.0.1 @ 31.01.2014]  
(+) Loading animation  
(E) Renamed some functions and cleaned code a little bit  
* > [0.9.0.0 @ 28.01.2014]  
(E) Application name changed to SMC (Skyrim Mod Combiner)  
(E) Application rebased to https://bitbucket.org/drigger/smc/